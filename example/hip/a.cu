
#define __global__ __attribute__((global))

__global__ void axpy(float a, float* x, float* y, int i) {
  y[i] = a * x[i];
}


