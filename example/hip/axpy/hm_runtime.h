// #include <stddef.h>
// #include <stdint.h>
// #include "__clang_hip_runtime_wrapper.h"

typedef unsigned size_t;
typedef int int32_t;
typedef unsigned uint32_t;

#define __host__ __attribute__((host))
#define __device__ __attribute__((device))
#define __global__ __attribute__((global))
#define __shared__ __attribute__((shared))
#define __constant__ __attribute__((constant))
#define __managed__ __attribute__((managed))
#define ATTR __attribute__((const))

typedef struct dim3 {
    uint32_t x;  ///< x
    uint32_t y;  ///< y
    uint32_t z;  ///< z
#ifdef __cplusplus
    constexpr __device__ dim3(uint32_t _x = 1, uint32_t _y = 1, uint32_t _z = 1) : x(_x), y(_y), z(_z){};
#endif
} dim3;

typedef struct hipStream *hipStream_t;
typedef enum hipError {} hipError_t;
int hipConfigureCall(dim3 gridSize, dim3 blockSize, size_t sharedSize = 0,
                     hipStream_t stream = 0);
extern "C" hipError_t __hipPushCallConfiguration(dim3 gridSize, dim3 blockSize,
                                                 size_t sharedSize = 0,
                                                 hipStream_t stream = 0);
extern "C" hipError_t hipLaunchKernel(const void *func, dim3 gridDim,
                                      dim3 blockDim, void **args,
                                      size_t sharedMem,
                                      hipStream_t stream);

template <typename F>
struct __HIP_Coordinates {
  using R = decltype(F{}(0));

  struct __X {
    __device__ operator R() const noexcept { return F{}(0); }
    __device__ R operator+=(const R& rhs) { return F{}(0) + rhs; }
  };
  struct __Y {
    __device__ operator R() const noexcept { return F{}(1); }
    __device__ R operator+=(const R& rhs) { return F{}(1) + rhs; }
  };
  struct __Z {
    __device__ operator R() const noexcept { return F{}(2); }
    __device__ R operator+=(const R& rhs) { return F{}(2) + rhs; }
  };

  static constexpr __X x{};
  static constexpr __Y y{};
  static constexpr __Z z{};
#ifdef __cplusplus
  __device__ operator dim3() const { return dim3(x, y, z); }
#endif

};

ATTR 
__device__ size_t __ockl_get_local_id(uint32_t dim)
{
    switch(dim) {
    case 0:
        return __builtin_amdgcn_workitem_id_x();
    case 1:
        return __builtin_amdgcn_workitem_id_y();
    case 2:
        return __builtin_amdgcn_workitem_id_z();
    default:
        return 0;
    }
}

struct __HIP_ThreadIdx {
  __device__
  uint32_t operator()(uint32_t x) const noexcept {
    return __ockl_get_local_id(x);
  }
};

static constexpr __HIP_Coordinates<__HIP_ThreadIdx> threadIdx{};
